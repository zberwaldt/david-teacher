---
title: About
url: /about
date: 2018-02-15T21:08:38.000Z
---
Hi, Dave Gillis here...

I am a life-long learner. So What does that really mean:

**_"What are you learning, Dave?"_**

Presently, I am exploring 3D Cubes with binary schemed edges made from a flatland laser-cut acrylic.

In my Design Thinking classes we are exploring engineering challenges in space optimization and solar energy capture. 

On a daily basis my students and I problem solve every interesting challenge we bump into. I also share life with my students, as it happens.
