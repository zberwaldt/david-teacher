---
title: Leader of the Curious
date: 2018-02-22T15:46:16.918Z
---
It is curiosity that Fuels Progress! It is wanting to know about the unknown that takes us to frontiers where new horizons become the known.... on the edge! Exhilarating!

As Leonardo Da Vinci is quoted in "How to Think Like Da Vinci", Curosita' is how we expand our knowledge and make connections of non-disparate things into a whole new thing.
